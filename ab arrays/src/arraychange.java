
public class arraychange {

	public static void main(String[] args) {
		int[] array = {8, 16, 32, 64, 128};
		double produkt =0;
		int zwspeicher =0;
		
		System.out.println("Das Array: ");
		for(int k=0;k<array.length;k++) {
			System.out.print(array[k]+" ");
		}
		System.out.println("\n");
		produkt = array[0]*array[1]*array[2]*array[3]*array[4];
		System.out.println("Das Produkt aller Array-Elemente ist: \n" + produkt);
		
		System.out.println("\n");
		
		zwspeicher = array[0];
		array[0] = array[4];
		array[4] = zwspeicher;
		zwspeicher = array[1];
		array[1] = array[3];
		array[3]= zwspeicher;
		
		System.out.println("Das Array invertiert: ");
		for(int i=0;i<array.length;i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println("\n");
		int[] array2 = new int[6];
		array2[0] = array[0];
		array2[1] = array[1];
		array2[2] = array[2];
		array2[3] = array[3];
		array2[4] = array[4];
		array2[5] = 4;
		
		System.out.println("Array erweitert um 1 mit der Zahl 4: ");
		
		for(int j=0;j<array2.length; j++) {
			System.out.print(array2[j]+" ");
		}
	}

}
