import java.util.Arrays;

public class ArrayHelper {

	public static void main(String[] args) {
		int[] zahlen = {1,2,3,4,5,6,7,8};
	
		String wort;

		wort = convertArrayToString(zahlen);
		System.out.println(wort);
	}

	public static String convertArrayToString(int[] zahlen) {
		String wort;
		
		wort = Arrays.toString(zahlen);
		
		return wort;
	}

}
