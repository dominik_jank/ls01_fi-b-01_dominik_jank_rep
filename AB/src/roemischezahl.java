import java.util.Scanner;

public class roemischezahl {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		System.out
				.println("Geben Sie die r�mische Zahl ein, die Sie umwandeln m�chten (Sie darf max. 3999 ergeben!): ");
		String romZahl = tastatur.next();
		int rzahl = 0;
		int anzahl = romZahl.length();
		char[] charrom = new char[anzahl];
		romZahl.getChars(0, anzahl, charrom, 0);
		int n = 0;
		
		// System.out.println(charrom[2]);
		for (int i = 0; i < anzahl; i++) {
			switch (charrom[n]) {
			case 'M':
				rzahl = rzahl + 1000;
				
				break;
			case 'D':
				rzahl = rzahl + 500;
				
				break;
			case 'C':
				rzahl = rzahl + 100;
				
				break;
			case 'L':
				rzahl = rzahl + 50;
				
				break;
			case 'X':
				rzahl =  rzahl+ 10;
				
				break;
			case 'V':
				rzahl = rzahl + 5;
				
				break;
			case 'I':
				rzahl = rzahl + 1;
				
				break;
			}
			n = n + 1;

		}

		if (romZahl.contains("IV") == true) {
			rzahl = rzahl - 2;
		} else if (romZahl.contains("IX") == true) {
			rzahl = rzahl - 2;
		} else if (romZahl.contains("XL") == true) {
			rzahl = rzahl - 20;
		} else if (romZahl.contains("XC") == true) {
			rzahl = rzahl - 20;
		} else if (romZahl.contains("CD") == true) {
			rzahl = rzahl - 200;
		} else if (romZahl.contains("CM") == true) {
			rzahl = rzahl - 200;
		}

		if (romZahl.contains("IIV") == true) {
			rzahl = rzahl - 1;
		} else if (romZahl.contains("IIX") == true) {
			rzahl = rzahl - 1;
		} else if (romZahl.contains("XXL") == true) {
			rzahl = rzahl - 10;
		} else if (romZahl.contains("XXC") == true) {
			rzahl = rzahl - 10;
		} else if (romZahl.contains("CCD") == true) {
			rzahl = rzahl - 100;
		} else if (romZahl.contains("CCM") == true) {
			rzahl = rzahl - 100;
		}

		if (romZahl.contains("IIIV") == true) {
			rzahl = rzahl - 1;
		} else if (romZahl.contains("IIIX") == true) {
			rzahl = rzahl - 1;
		} else if (romZahl.contains("XXXL") == true) {
			rzahl = rzahl - 10;
		} else if (romZahl.contains("XXXC") == true) {
			rzahl = rzahl - 10;
		} else if (romZahl.contains("CCCD") == true) {
			rzahl = rzahl - 100;
		} else if (romZahl.contains("CCCM") == true) {
			rzahl = rzahl - 100;
		}

		if ((romZahl.contains("I") || romZahl.contains("V") || romZahl.contains("X") || romZahl.contains("L")
				|| romZahl.contains("C") || romZahl.contains("D") || romZahl.contains("M")) == false) {
			System.out.println("Fehler!");
			System.exit(0);
		}

		if ((romZahl.contains("IIII") || romZahl.contains("VV") || romZahl.contains("XXXX") || romZahl.contains("LL")
				|| romZahl.contains("CCCC") || romZahl.contains("DD") || romZahl.contains("MMMM")
				|| romZahl.contains("CMC") || romZahl.contains("IVI") || romZahl.contains("IXI")
				|| romZahl.contains("XLX") || romZahl.contains("XCX") || romZahl.contains("CDC")) == true) {
			System.out.println("Fehler!");
			System.exit(0);
		}
		
		String wort = vergleich(romZahl, rzahl);
		if(romZahl.equals(wort) == true) {
			System.out.println(rzahl);
			
		} else {
			System.out.println("Fehler!");
			System.exit(0);
		}
		
		
	}
	
	
	public static String vergleich(String romZahl, int rzahl) {
		
		int m = rzahl;
		System.out.println(rzahl);
		String wort = "";
		while(m >= 1000) {
			m= m - 1000;
			wort = wort + "M";
			System.out.println(m);
	}
	while (m>= 900) {
		m=m-900;
		wort=wort+"CM";
		System.out.println(m);
	}
	
	while (m >= 500) {
		m= m-500;
		wort = wort + "D";
		System.out.println(m);
	}
	
	while (m>= 400) {
		m=m-400;
		wort=wort+"CD";
		System.out.println(m);
	}
	
	while (m >= 100) {
		m= m-100;
		wort = wort + "C";
		System.out.println(m);
	}
	while (m>= 90) {
		m=m-90;
		wort=wort+"XC";
		System.out.println(m);
	}
	
	
	while (m >= 50) {
		m= m-50;
		wort = wort + "L";
		System.out.println(m);
	}
	
	while (m>= 40) {
		m=m-40;
		wort=wort+"XL";
		System.out.println(m);
	}
	
	
	while (m >= 10) {
		m= m-10;
		wort = wort + "X";
		System.out.println(m);
	}
	
	while (m >= 9) {
		m= m-9;
		wort = wort + "IX";
		System.out.println(m);
	}
	
	while (m >= 5) {
		m= m-5;
		wort = wort + "V";
		System.out.println(m);
	}
	
	while (m>= 4) {
		m=m-4;
		wort=wort+"IV";
		System.out.println(m);
	}
	
	
	while (m >= 1) {
		m= m-1;
		wort = wort + "I";
		System.out.println(m);
	}
	System.out.println(wort);
		return wort;
	}
}
