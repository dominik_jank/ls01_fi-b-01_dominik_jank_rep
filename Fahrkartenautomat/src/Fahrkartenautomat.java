﻿import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat
{
    public static void main(String[] args) throws InterruptedException
    {
    	
       Scanner tastatur = new Scanner(System.in);
      
       //System.out.println("hallo");
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int nachfrage = 1;
       int anzahltickets;
       
       
       do {
    	   
    	   System.out.println("Guten Tag.\nWas brauchen Sie?");   
       //Methode Fahrkarten
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       //Methode-Bezahlung
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       //Fahrkartenausgabe
       fahrkartenAusgeben();
       
       
       //Rückgeldausgabe
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
       Thread.sleep(2000);
       System.out.println("\n\n\n\n\n");
       } while(nachfrage == 1);
       
      
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag=0.0;
    	System.out.println("Wählen Sie eine Fahrkartenart aus.");
    	/*System.out.println("Geben Sie die Nummer ein, von der Fahrkarte (Bereich Berlin), die Sie bestellen möchten.\n");
    	System.out.println("Zur Verfügung stehen: \n "
    			+ "Einzelfahrschein Regeltarif AB [2,90€] (1) \n "
    			+ "Tageskarte Regeltarif AB [8,60€] (2) \n "
    			+ "Kleingruppen-Tageskarte Regeltarif AB [23,50€] (3)");*/
    	
    	String[][] fahrkartentyparray = new String[11][3];
    	fahrkartentyparray[0][0] = "Auswahlnummer";
    	fahrkartentyparray[0][1] = "Bezeichnung";
    	fahrkartentyparray[0][2] = "Preis in Euro";
    	fahrkartentyparray[1][0] = "1";
    	fahrkartentyparray[2][0] = "2";
    	fahrkartentyparray[3][0] = "3";
    	fahrkartentyparray[4][0] = "4";
    	fahrkartentyparray[5][0] = "5";
    	fahrkartentyparray[6][0] = "6";
    	fahrkartentyparray[7][0] = "7";
    	fahrkartentyparray[8][0] = "8";
    	fahrkartentyparray[9][0] = "9";
    	fahrkartentyparray[10][0] = "10";
    	fahrkartentyparray[1][1] = "Einzelfahrschein Berlin AB";
    	fahrkartentyparray[2][1] = "Einzelfahrschein Berlin BC";
    	fahrkartentyparray[3][1] = "Einzelfahrschein Berlin ABC";
    	fahrkartentyparray[4][1] = "Kurzstrecke";
    	fahrkartentyparray[5][1] = "Tageskarte Berlin AB";
    	fahrkartentyparray[6][1] = "Tageskarte Berlin BC";
    	fahrkartentyparray[7][1] = "Tageskarte Berlin ABC";
    	fahrkartentyparray[8][1] = "Kleingruppen-Tageskarte Berlin AB";
    	fahrkartentyparray[9][1] = "Kleingruppen-Tageskarte Berlin BC";
    	fahrkartentyparray[10][1] = "Kleingruppen-Tageskarte Berlin ABC";
    	fahrkartentyparray[1][2] = "2.90";
    	fahrkartentyparray[2][2] = "3.30";
    	fahrkartentyparray[3][2] = "3.60";
    	fahrkartentyparray[4][2] = "1.90";
    	fahrkartentyparray[5][2] = "8.60";
    	fahrkartentyparray[6][2] = "9.00";
    	fahrkartentyparray[7][2] = "9.60";
    	fahrkartentyparray[8][2] = "23.50";
    	fahrkartentyparray[9][2] = "24.30";
    	fahrkartentyparray[10][2] = "24.90";
    	
    	for(int i=0;i<11;i++) {
    		System.out.printf("%-20s %-40s %-15s\n",fahrkartentyparray[i][0], fahrkartentyparray[i][1], fahrkartentyparray[i][2]);
    		
    	}

    	
    	
    	System.out.println("Ihre Wahl: ");
    	int fahrkartentyp = tastatur.nextInt();
    	//double zuZahlenderBetrag = tastatur.nextDouble();
        //int counter=0;
        int counter2=0;
        while(fahrkartentyp > 10 || fahrkartentyp <=0) {
        	System.out.println(">>falsche Eingabe<<");
        	System.out.println("Geben Sie eine gültige Fahrkartentypnummer ein. (1 bis 10)");
        	fahrkartentyp = tastatur.nextInt();
        	System.out.println("Ihre Wahl: " + fahrkartentyp);
        	
        	
        	/*zuZahlenderBetrag = tastatur.nextDouble();
        	counter = counter +1;
        	if(counter == 5) {
        		System.out.println("Aufgrund deiner Fehlversuche wurde dein Ticketpreis auf 1€ gesetzt!");
        		zuZahlenderBetrag=1;
        	}*/
        }
        
        
        switch (fahrkartentyp) {
        case 1:
			zuZahlenderBetrag = zuZahlenderBetrag + 2.90;
			break;
			
        case 2: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 3.30;
        	break;
        	
        case 3: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 3.60;
        	break;
        	
        case 4: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 1.90;
        	break;
        	
        case 5: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 8.60;
        	break;
        	
        case 6: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 9.00;
        	break;
        	
        case 7: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 9.60;
        	break;
        	
        case 8: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 23.50;
        	break;
        	
        case 9: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 24.30;
        	break;
        	
        case 10: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 24.90;
        	break;
        	
        }
        
        
        System.out.println("Wie viele Tickets wollen Sie? Min.:1, Max.:10");
       
        int AnzahlTickets = tastatur.nextInt();
       /* if(AnzahlTickets > 10 || AnzahlTickets < 1) {
        	System.out.println("Sie haben eine nicht gültige Anzahl eingegeben! \nDie Anzahl wurde auf 1 gesetzt.");
        	AnzahlTickets = 1;
        };*/
        while (AnzahlTickets > 10 || AnzahlTickets < 1) {
        	System.out.println("Sie haben eine nicht gültige Anzahl eingegeben!\nGeben Sie eine gültige Anzahl ein:");
        	AnzahlTickets = tastatur.nextInt();
        	counter2=counter2+1;
        	if(counter2>=5) {
        		System.out.println("Deine Anzahl wurde standardmäßig auf 1 gesetzt.");
        		AnzahlTickets=1;
        	}
        };
        zuZahlenderBetrag = zuZahlenderBetrag*AnzahlTickets;
        System.out.printf("Die Zwischensumme beträgt: %.2f € \n\n", zuZahlenderBetrag);
        
        
        	do {
        		double wticket = 0;
        		System.out.println("Wählen Sie eine Fahrkartenart aus.");
            	/*System.out.println("Geben Sie die Nummer ein, von der Fahrkarte (Bereich Berlin), die Sie bestellen möchten.\n");
            	System.out.println("Zur Verfügung stehen: \n "
            			+ "Einzelfahrschein Regeltarif AB [2,90€] (1) \n "
            			+ "Tageskarte Regeltarif AB [8,60€] (2) \n "
            			+ "Kleingruppen-Tageskarte Regeltarif AB [23,50€] (3) \n "
            			+ "Bezahlen (9)");*/
            	
        		for(int i=0;i<11;i++) {
            		System.out.printf("%-20s %-40s %-15s\n",fahrkartentyparray[i][0], fahrkartentyparray[i][1], fahrkartentyparray[i][2]);
            		
            	}
        		System.out.println("Wenn Sie bezahlen wollen, geben Sie die 11 ein.");
        		
        		fahrkartentyp = tastatur.nextInt();
            	System.out.println("Ihre Wahl: " + fahrkartentyp + "\n");
        
                counter2=0;
                while(fahrkartentyp != 1 && fahrkartentyp != 2 && fahrkartentyp != 3 && fahrkartentyp != 4 && fahrkartentyp != 5 && fahrkartentyp != 6 && fahrkartentyp != 7 && fahrkartentyp != 8 && fahrkartentyp != 9 && fahrkartentyp != 10 && fahrkartentyp != 11) {
                	System.out.println(">>falsche Eingabe<<");
                	System.out.println("Geben Sie eine gültige Fahrkartentypnummer ein. (1 bis 10) oder wählen Sie bezahlen (11)");
                	fahrkartentyp = tastatur.nextInt();
                	System.out.println("Ihre Wahl: " + fahrkartentyp + "\n");
                	
                }
                
                
                switch (fahrkartentyp) {
                case 1:
        			zuZahlenderBetrag = zuZahlenderBetrag + 2.90;
        			break;
        			
                case 2: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 3.30;
                	break;
                	
                case 3: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 3.60;
                	break;
                	
                case 4: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 1.90;
                	break;
                	
                case 5: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 8.60;
                	break;
                	
                case 6: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 9.00;
                	break;
                	
                case 7: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 9.60;
                	break;
                	
                case 8: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 23.50;
                	break;
                	
                case 9: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 24.30;
                	break;
                	
                case 10: 
                	zuZahlenderBetrag = zuZahlenderBetrag + 24.90;
                	break;
                	
                }
                
                if(fahrkartentyp != 11) {
                	
                
                System.out.println("Wie viele Tickets wollen Sie? Min.:1, Max.:10");
               
                AnzahlTickets = tastatur.nextInt();
                while (AnzahlTickets > 10 || AnzahlTickets < 1) {
                	System.out.println("Sie haben eine nicht gültige Anzahl eingegeben!\nGeben Sie eine gültige Anzahl ein:");
                	AnzahlTickets = tastatur.nextInt();
                	counter2=counter2+1;
                	if(counter2>=5) {
                		System.out.println("Deine Anzahl wurde standardmäßig auf 1 gesetzt.");
                		AnzahlTickets=1;
                	}
                };
                wticket = wticket*AnzahlTickets;
                zuZahlenderBetrag = zuZahlenderBetrag+wticket;
                System.out.printf("Die Zwischensumme beträgt: %.2f € \n\n", zuZahlenderBetrag);
                }
        		
        	} while (fahrkartentyp != 11);
        	
        
        if(fahrkartentyp == 11) {
        	System.out.println("Der Bezahlvorgang wird eingeleitet.\n");
        }
    	
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen:%.2f €\n" ,(zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	int millisekunde = 250;
    	warte(millisekunde);
        
        System.out.println("\n\n");
    	
    }
    
    public static void warte(int millisekunde) {
    	 
    	//for (int i = 0; i < 8; i++)
    	//{
            //System.out.print("=");
            try {
  			Thread.sleep(millisekunde);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         //}
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO. \n",rückgabebetrag);
      	   System.out.println("Wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  //System.out.println("2 EURO");
          	  int betrag = 2;
          	  String einheit = "EURO";
          	  muenzeAusgeben(betrag, einheit);
          	 
          	  rückgabebetrag=rückgabebetrag-2.0;
             }
             while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
             {
          	  //System.out.println("1 EURO");
          	  int betrag = 1;
        	  String einheit = "EURO";
        	  muenzeAusgeben(betrag, einheit);
          	  rückgabebetrag=rückgabebetrag-1.0;
             }
             while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
             {
          	  //System.out.println("50 CENT");
            	int betrag = 50;
             	String einheit = "CENT";
             	muenzeAusgeben(betrag, einheit);
          	 
          	  rückgabebetrag=rückgabebetrag-0.50;
             }
             while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
             {
          	  //System.out.println("20 CENT");
            	int betrag = 20;
              	String einheit = "CENT";
              	muenzeAusgeben(betrag, einheit); 
          	  rückgabebetrag=Math.round(rückgabebetrag*100.0)/100.0;
          	  rückgabebetrag=rückgabebetrag-0.20;
             }
             while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
             {
          	  //System.out.println("10 CENT");
            	int betrag = 10;
              	String einheit = "CENT";
              	muenzeAusgeben(betrag, einheit);
          	  rückgabebetrag=Math.round(rückgabebetrag*100.0)/100.0;
          	  rückgabebetrag=rückgabebetrag-0.10;
             }
             while(rückgabebetrag >= 0.050)// 5 CENT-Münzen
             {
          	  //System.out.println("5 CENT");
            	int betrag = 5;
              	String einheit = "CENT";
              	muenzeAusgeben(betrag, einheit);
          	  rückgabebetrag=Math.round(rückgabebetrag*100.0)/100.0;
          	  rückgabebetrag=rückgabebetrag-0.050;
             }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.printf("\n%2d " + einheit, betrag);
    }

}